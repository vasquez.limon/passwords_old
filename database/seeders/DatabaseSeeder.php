<?php

namespace Database\Seeders;

use App\Models\Credential;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Credential::factory(10)->create();
        // $this->call(ResourceTableSeeder::class);
    }
}
